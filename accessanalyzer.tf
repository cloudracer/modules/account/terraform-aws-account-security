resource "aws_accessanalyzer_analyzer" "this" {
  analyzer_name = "account"
  type          = length(regexall("(?i)audit", var.account_name)) == 0 ? "ACCOUNT" : "ORGANIZATION"
}
