resource "aws_securityhub_member" "member" {
  count    = length(regexall("(?i)audit", var.account_name)) == 0 ? 1 : 0
  provider = aws.audit

  account_id = var.aws_organizations_account_resource.id
  email      = var.aws_organizations_account_resource.email
  invite     = false

  lifecycle {
    ignore_changes = [invite, email]
  }
}

# resource "aws_securityhub_invite_accepter" "invitee" {
#   master_id = data.aws_caller_identity.audit.id
# }
