# Account Security Module

This module consolidates the following modules:

- terraform-aws-account-security-config
- terraform-aws-account-security-guardduty
- terraform-aws-account-region-security

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.audit"></a> [aws.audit](#provider\_aws.audit) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_accessanalyzer_analyzer.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/accessanalyzer_analyzer) | resource |
| [aws_config_aggregate_authorization.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_aggregate_authorization) | resource |
| [aws_config_configuration_recorder.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_configuration_recorder) | resource |
| [aws_config_configuration_recorder_status.enable](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_configuration_recorder_status) | resource |
| [aws_config_delivery_channel.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_delivery_channel) | resource |
| [aws_default_network_acl.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_network_acl) | resource |
| [aws_default_route_table.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_route_table) | resource |
| [aws_default_security_group.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group) | resource |
| [aws_default_subnet.default_azs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_subnet) | resource |
| [aws_default_vpc.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_vpc) | resource |
| [aws_default_vpc_dhcp_options.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_vpc_dhcp_options) | resource |
| [aws_ebs_encryption_by_default.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ebs_encryption_by_default) | resource |
| [aws_flow_log.default_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/flow_log) | resource |
| [aws_guardduty_detector.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/guardduty_detector) | resource |
| [aws_guardduty_invite_accepter.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/guardduty_invite_accepter) | resource |
| [aws_guardduty_member.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/guardduty_member) | resource |
| [aws_iam_role.aws_config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.config_s3_delivery](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.aws_config_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_account_public_access_block.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_account_public_access_block) | resource |
| [aws_securityhub_member.member](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/securityhub_member) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_caller_identity.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_name"></a> [account\_name](#input\_account\_name) | Name of the current account. | `string` | n/a | yes |
| <a name="input_all_aws_organizations_account_resources"></a> [all\_aws\_organizations\_account\_resources](#input\_all\_aws\_organizations\_account\_resources) | All the AWS Orginzations resources | `any` | n/a | yes |
| <a name="input_audit_guardduty_detector_id"></a> [audit\_guardduty\_detector\_id](#input\_audit\_guardduty\_detector\_id) | AWS GuardDuty Detector ID of the Audit Account | `string` | n/a | yes |
| <a name="input_aws_config_bucket_name"></a> [aws\_config\_bucket\_name](#input\_aws\_config\_bucket\_name) | Name of the AWS Config Bucket. | `string` | `""` | no |
| <a name="input_aws_organizations_account_resource"></a> [aws\_organizations\_account\_resource](#input\_aws\_organizations\_account\_resource) | The AWS Orginzations resource for this account | `any` | n/a | yes |
| <a name="input_enable_config"></a> [enable\_config](#input\_enable\_config) | Enable / Disable AWS Config | `bool` | `false` | no |
| <a name="input_enable_ebs_default_encryption"></a> [enable\_ebs\_default\_encryption](#input\_enable\_ebs\_default\_encryption) | Enable / Disable EBS default encryption | `bool` | `false` | no |
| <a name="input_enable_guardduty"></a> [enable\_guardduty](#input\_enable\_guardduty) | Enable / Disable AWS GuardDuty | `bool` | `false` | no |
| <a name="input_flow_logs_bucket_arn"></a> [flow\_logs\_bucket\_arn](#input\_flow\_logs\_bucket\_arn) | VPC FlowLogs Bucket | `string` | n/a | yes |
| <a name="input_general_group_naming_construct"></a> [general\_group\_naming\_construct](#input\_general\_group\_naming\_construct) | Defines the Naming of AWS IAM Groups. You need to add: <ACCOUNT>, <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_general_policy_naming_construct"></a> [general\_policy\_naming\_construct](#input\_general\_policy\_naming\_construct) | Defines the Naming of AWS IAM Policies. You need to add: <ACCOUNT>, <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_general_role_naming_construct"></a> [general\_role\_naming\_construct](#input\_general\_role\_naming\_construct) | Defines the Naming of AWS IAM Roles. You need to add: <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_lock_down_default_vpc"></a> [lock\_down\_default\_vpc](#input\_lock\_down\_default\_vpc) | Remove the routes and NALC entries from the default VPC to effectively disable it, default: True | `bool` | `true` | no |
| <a name="input_organization"></a> [organization](#input\_organization) | Complete name of the organisation | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Region for the Module | `string` | n/a | yes |
| <a name="input_s3_block_public_acls"></a> [s3\_block\_public\_acls](#input\_s3\_block\_public\_acls) | value | `bool` | `true` | no |
| <a name="input_s3_block_public_policy"></a> [s3\_block\_public\_policy](#input\_s3\_block\_public\_policy) | value | `bool` | `true` | no |
| <a name="input_s3_ignore_public_acls"></a> [s3\_ignore\_public\_acls](#input\_s3\_ignore\_public\_acls) | value | `bool` | `true` | no |
| <a name="input_s3_restrict_public_buckets"></a> [s3\_restrict\_public\_buckets](#input\_s3\_restrict\_public\_buckets) | value | `bool` | `true` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag that should be applied to all resources. | `map(string)` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
