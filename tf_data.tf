# General
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}
data "aws_caller_identity" "audit" {
  provider = aws.audit
}
