# Create guardduty detector in every AWS account and in every desired region
resource "aws_guardduty_detector" "this" {
  count = var.enable_guardduty && length(regexall("(?i)audit", var.account_name)) == 0 ? 1 : 0

  enable = var.enable_guardduty
  tags   = local.tags
}

# if this is not the audit account, invite this account to the audit account
resource "aws_guardduty_member" "this" {
  count    = var.enable_guardduty && length(regexall("(?i)audit", var.account_name)) == 0 ? 1 : 0
  provider = aws.audit

  account_id         = var.aws_organizations_account_resource.id
  detector_id        = var.audit_guardduty_detector_id
  email              = var.aws_organizations_account_resource.email
  invite             = true
  invitation_message = "Please accept AWS GuardDuty invitation"
}

resource "aws_guardduty_invite_accepter" "this" {
  count      = var.enable_guardduty && length(regexall("(?i)audit", var.account_name)) == 0 ? 1 : 0
  depends_on = [aws_guardduty_member.this[0]]

  detector_id       = aws_guardduty_detector.this[0].id
  master_account_id = data.aws_caller_identity.audit.account_id
}
