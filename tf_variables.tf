// Organization specific tags
variable "organization" {
  type        = string
  description = "Complete name of the organisation"
}

variable "aws_organizations_account_resource" {
  description = "The AWS Orginzations resource for this account"
}

variable "all_aws_organizations_account_resources" {
  description = "All the AWS Orginzations resources"
}

variable "account_name" {
  type        = string
  description = "Name of the current account."
}

variable "region" {
  type        = string
  description = "Region for the Module"
}

# Service specific Variables

// Region Security
variable "enable_ebs_default_encryption" {
  type        = bool
  description = "Enable / Disable EBS default encryption"
  default     = false
}

// GuardDuty
variable "enable_guardduty" {
  type        = bool
  description = "Enable / Disable AWS GuardDuty"
  default     = false
}

variable "audit_guardduty_detector_id" {
  type        = string
  description = "AWS GuardDuty Detector ID of the Audit Account"
}

// Config
variable "enable_config" {
  type        = bool
  description = "Enable / Disable AWS Config"
  default     = false
}

variable "aws_config_bucket_name" {
  type        = string
  description = "Name of the AWS Config Bucket."
  default     = ""
}

variable "lock_down_default_vpc" {
  type        = bool
  description = "Remove the routes and NALC entries from the default VPC to effectively disable it, default: True"
  default     = true
}

# Variables for Tags

variable "system" { // Needed for Tags
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" { // Needed for Tags
  type        = string
  description = "Name of a dedicated system or application"
}
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

# IAM
variable "general_role_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Roles. You need to add: <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end."
}

variable "general_policy_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Policies. You need to add: <ACCOUNT>, <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end."
}

variable "general_group_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Groups. You need to add: <ACCOUNT>, <SYSTEM> and <ACCESSLEVEL> in the string! We will always add -tflz at the end."
}

variable "flow_logs_bucket_arn" {
  type        = string
  description = "VPC FlowLogs Bucket"
}

/* S3 Block Public Access */
variable "s3_block_public_acls" {
  description = "value"
  type        = bool
  default     = true
}

variable "s3_block_public_policy" {
  description = "value"
  type        = bool
  default     = true
}

variable "s3_ignore_public_acls" {
  description = "value"
  type        = bool
  default     = true
}

variable "s3_restrict_public_buckets" {
  description = "value"
  type        = bool
  default     = true
}
