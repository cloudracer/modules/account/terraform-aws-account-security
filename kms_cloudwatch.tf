resource "aws_kms_key" "this" {
  description             = format("%s customer managed key for region %s.", data.aws_caller_identity.current.account_id, data.aws_region.current.name)
  deletion_window_in_days = 7
  is_enabled              = true
  key_usage               = "ENCRYPT_DECRYPT"
  enable_key_rotation     = true
  policy = templatefile("${path.module}/templates/cloudwatch_kms.tpl", {
    ACCOUNT_ID  = data.aws_caller_identity.current.account_id
    REGION_NAME = data.aws_region.current.name
  })

  tags = local.tags
}

resource "aws_kms_alias" "this" {
  name          = format("alias/%s/cloudwatch", data.aws_region.current.name)
  target_key_id = aws_kms_key.this.key_id
}
