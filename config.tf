resource "aws_config_configuration_recorder" "this" {
  count = var.enable_config ? 1 : 0

  name     = format("configuration-recorder-%s", var.region)
  role_arn = aws_iam_role.aws_config[0].arn

  recording_group {
    all_supported                 = true
    include_global_resource_types = true
  }
}

resource "aws_config_delivery_channel" "s3" {
  count = var.enable_config ? 1 : 0

  name           = "s3"
  s3_bucket_name = var.aws_config_bucket_name
  depends_on     = [aws_config_configuration_recorder.this[0]]
}

resource "aws_config_configuration_recorder_status" "enable" {
  count = var.enable_config ? 1 : 0

  name       = aws_config_configuration_recorder.this[0].name
  is_enabled = true
  depends_on = [aws_config_delivery_channel.s3[0]]
}

# Aggreation to Audit Accounts
resource "aws_config_aggregate_authorization" "this" {
  # eu-north-1 (Stockholm) doesn't support config aggregations yet
  count = var.enable_config && length(regexall("(?i)audit", var.account_name)) == 0 && var.region != "eu-north-1" ? 1 : 0

  account_id = data.aws_caller_identity.audit.account_id
  region     = var.region
  depends_on = [aws_config_configuration_recorder.this[0]]
}
