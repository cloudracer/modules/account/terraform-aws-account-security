resource "aws_iam_role" "aws_config" {
  count = var.enable_config ? 1 : 0

  name = format("%s-tflz", replace(replace(var.general_role_naming_construct, "<SYSTEM>", "awsconfig"), "<ACCESSLEVEL>", "all"))
  assume_role_policy = templatefile("${path.module}/templates/iam/assume_role.tpl", {
    PRINCIPAL_TYPE = "Service",
    PRINCIPAL      = "config.amazonaws.com"
  })

  tags = local.tags
}

resource "aws_iam_role_policy" "config_s3_delivery" {
  count = var.enable_config ? 1 : 0

  name = format("%s-tflz", replace(replace(replace(var.general_policy_naming_construct, "<ACCOUNT>-", ""), "<SYSTEM>", "awsconfig"), "<ACCESSLEVEL>", "s3delivery"))
  role = aws_iam_role.aws_config[0].id

  policy = templatefile("${path.module}/templates/iam/config_s3_delivery.tpl", {
    CONFIG_BUCKET = var.aws_config_bucket_name,
    ACCOUNT_ID    = data.aws_caller_identity.current.account_id
  })
}

resource "aws_iam_role_policy_attachment" "aws_config_role" {
  count = var.enable_config ? 1 : 0

  role       = aws_iam_role.aws_config[0].id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWS_ConfigRole" #Changed Role according to https://docs.aws.amazon.com/config/latest/developerguide/security-iam-awsmanpol.html
}
