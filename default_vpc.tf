data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_default_vpc" "default" {
  count = var.lock_down_default_vpc ? 1 : 0

  tags = {
    Name = "Default VPC (disabled)"
  }
}

resource "aws_default_subnet" "default_azs" {
  for_each = var.lock_down_default_vpc ? toset(data.aws_availability_zones.available.names) : toset([])

  availability_zone       = each.value
  map_public_ip_on_launch = false

  tags = {
    Name = "Default Subnet AZ ${each.value} (disabled)"
  }
}

// FlowLogs (to see if any Traffic happens)
resource "aws_flow_log" "default_flowlogs" {
  count = var.lock_down_default_vpc ? 1 : 0

  log_destination      = var.flow_logs_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_default_vpc.default.0.id

  tags = local.tags
}

// Restrict default security group
resource "aws_default_security_group" "default" {
  count = var.lock_down_default_vpc ? 1 : 0

  vpc_id = aws_default_vpc.default.0.id

  tags = {
    Name = "Default SG (disabled)"
  }
}

// Remove all routes from default route table
resource "aws_default_route_table" "default" {
  count = var.lock_down_default_vpc ? 1 : 0

  default_route_table_id = aws_default_vpc.default.0.default_route_table_id

  tags = {
    Name = "Default RTB (disabled)"
  }
}

// Default NACL
resource "aws_default_network_acl" "default" {
  count = var.lock_down_default_vpc ? 1 : 0

  default_network_acl_id = aws_default_vpc.default.0.default_network_acl_id

  # no rules defined, deny all traffic in this ACL
  tags = {
    Name = "Default NACL (disabled)"
  }

  lifecycle {
    ignore_changes = [subnet_ids]
  }
}

resource "aws_default_vpc_dhcp_options" "default" {
  count = var.lock_down_default_vpc ? 1 : 0

  owner_id = data.aws_caller_identity.current.account_id

  tags = {
    Name = "Default DHCP Option Set (disabled)"
  }
}
