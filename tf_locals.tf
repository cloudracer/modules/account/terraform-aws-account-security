locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-region-security"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-regions-security"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)
}
