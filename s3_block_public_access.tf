resource "aws_s3_account_public_access_block" "this" {
  block_public_acls       = var.s3_block_public_acls
  block_public_policy     = var.s3_block_public_policy
  ignore_public_acls      = var.s3_ignore_public_acls
  restrict_public_buckets = var.s3_restrict_public_buckets

  # You can either use Lifecycle (to control from AWS Management Console or you use the variables from Terraform)
  # lifecycle {
  #   ignore_changes = [block_public_acls, block_public_policy, ignore_public_acls, restrict_public_buckets]
  # }
}
